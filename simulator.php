<?php
include 'utils.php';
$SLOW = FALSE; // set this to true to run simulation slowly
$REAL_TIME = FALSE; // set this to true to try to match gps time stamp

echo "Simulator Started \n";

date_default_timezone_set("Australia/Sydney");

$con = pg_connect("host=220.239.171.180 port=5432 dbname=thesis_db user=service password=serve");

if(!$con){
        die('Could not connect: ' . mysql_error());
}
echo "Connecion Successful\n";
		 
$file = "GPSData_1505_04_07_2012.m";

$fh = fopen($file,'r');
fgets($fh); // read the first line
	
$start = time();
$prev_gps = 0;
$new_gps = 0;

while(!feof($fh)){
	$line = fgets($fh);
	$line = str_replace(';', '', $line);
	// split on 1 or more comma or space characters
	$array = preg_split("/[\s,]+/", $line);
	$easting_r = $array[1];
	$northing_r = $array[2];
	$height_r = $array[3];
	$easting_l = $array[4];
	$northing_l = $array[5];
	$height_l = $array[6];
	$heading = $array[7];
	$timestamp = $array[8];
	$xs_acl_x = $array[9];
	$xs_acl_y = $array[10];
	$xs_acl_z = $array[11];
	$xs_gyro_x = $array[12];
	$xs_gyro_y = $array[13];
	$xs_gyro_z = $array[14];
	$xs_roll = $array[15];
	$xs_pitch = $array[16];
	$xs_yaw = $array[17];
	$xs_time = $array[18];
	$time = round(microtime(true) * 1000); // time in ms since epoch
	$speed = rand(1,5);
	$fuel = 20;
	$eng_temp = 60;

	// create parameters array
	$params = array($easting_r,$northing_r,$height_r,
		$easting_l, $northing_l,$height_l,$heading,$timestamp,$xs_acl_x,
		$xs_acl_y,$xs_acl_z,$xs_gyro_x,$xs_gyro_y,$xs_gyro_z,$xs_roll,
		$xs_pitch,$xs_yaw,$xs_time,$time,$speed,$fuel,$eng_temp);
	
	// generate query string	
	$query = "INSERT into jd1_raw(easting_r,northing_r,height_r,";
	$query .= "easting_l,northing_l,height_l,heading,gps_time,";//)";

	$query .= "xs_acl_x,xs_acl_y,";
	$query .= "xs_acl_z, xs_gyro_x, xs_gyro_y, xs_gyro_z, xs_roll, xs_pitch,";
	$query .= "xs_yaw,xs_time,systime,speed,fuel,eng_temp) "; 
	$query .= "VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20,$21,$22)";
	if($northing_r != NULL){
		$result = pg_query_params($con,$query,$params);
	}
	
	if($SLOW){
		sleep(1);
		echo "sent\n";
	}
	if($REAL_TIME){
		$prev_gps = $new_gps;
		$new_gps = $timestamp;
		if($prev_gps != 0){
			$sleep_ms = $new_gps-$prev_gps;
			$sleep_us = $sleep_ms*1000; 
			echo $sleep_ms."\n";
			usleep($sleep_us);// micro seconds
		}
	}
}

$end = time();
echo "Elapsed Time: ". ($end-$start)." s";

pg_close($con);
echo "\n Simulator Ended\n";
		
?>
