<?php
function test(){
	echo 'hello';
}

function printArray($array){
	echo "Elements: (". count($array).")\n";
	echo "Contents\n[";
	foreach ($array as $a){
		echo $a.", ";
	}
	echo "]\n";
}

function getCurrentTime(){
	list($milliseconds,$seconds) = split(" ", microtime());
	return date("d-m-Y H:i:s").".".substr($milliseconds, 2, 3);
}
?>